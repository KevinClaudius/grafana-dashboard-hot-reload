module example.com/dashboard-loader/m/v2

go 1.13

require (
	github.com/sergi/go-diff v1.1.0 // indirect
	golang.org/x/crypto v0.0.0-20200128174031-69ecbb4d6d5d // indirect
	golang.org/x/net v0.0.0-20200202094626-16171245cfb2 // indirect
	golang.org/x/sys v0.0.0-20200202164722-d101bd2416d5 // indirect
	gopkg.in/src-d/go-git.v4 v4.13.1 // indirect
)
