package main

import (
	"fmt"
	"io"
	"os"
	"time"

	"gopkg.in/src-d/go-git.v4"
	"path/filepath"
)

// TODO:
// * some way of configuring which git repos to load, which folder in each repo that dashboards live, folder name in Grafana
// * Pass in dashboards destination directory into which to copy
// TODO: folder name in grafana is not configured by sidecar but by provisioning, might need to restart Grafana if we add
//       more dashboard folders.
func main() {
	// TODO: instrument this code with Prometheus metrics
	// * Histogram for clone times
	// * Histogram for polling a repo
	// * Histogram for time copying dashboards from a repo (per dir?  per file?)
	// * Counter for number of dashboards loaded (increment on first load and when changed?  Only copy when changed?)
	// TODO: pass in name of file location in which to check out repos (can be an ephemeral docker volume, later PV for efficiency?)
	gitURI := "https://github.com/KevinClaudius/grafana-dashboards-example"
	localRepoFolder := "/repos/grafana-dashboards-example"
	dashboardProvisioningFolder := "/tmp/dashboards"
	err := checkoutRepo(gitURI, localRepoFolder)
	if err != nil {
		// TODO: what kind of errors can be handled for repo checkout?  Should we retry?
		fmt.Println(err)
		os.Exit(1)
	}
	err = loadDashboards(localRepoFolder, dashboardProvisioningFolder)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// TODO: infinite loop, poll git repos for changes or have some way of notifying this code if changes have occurred?
	time.Sleep(30 * time.Second)
}

func loadDashboards(localRepoFolder, dest string) error {
	localRepoFolder = filepath.Clean(localRepoFolder)
	dest = filepath.Clean(dest)
	files, err := filepath.Glob(fmt.Sprintf("%s/*.json", localRepoFolder))
	if err != nil {
		// TODO: what kind of error can Glob return?  Is there some kind of validation of my input
		// that I can do to prevent this?
		return err
	}
	fmt.Printf("Provisioning %d dashboard(s)...\n", len(files))
	for _, f := range files {
		fmt.Println(f)
		err := copyFileContents(f, fmt.Sprintf("%s/%s", dest, filepath.Base(f)))
		if err != nil {
			return err
		}
	}
	return nil
}

func checkoutRepo(uri, dest string) (err error) {
	// TODO: clean up and re-checkout if a repo is not working
	// TODO: specify which branch of a repo to use (assume master for now)
	_, err = git.PlainClone(dest, false, &git.CloneOptions{
		URL:      uri,
		Progress: os.Stdout,
	})
	return
}

// https://stackoverflow.com/a/21067803
// copyFileContents copies the contents of the file named src to the file named
// by dst. The file will be created if it does not already exist. If the
// destination file exists, all it's contents will be replaced by the contents
// of the source file.
func copyFileContents(src, dst string) (err error) {
	in, err := os.Open(src)
	if err != nil {
		return
	}
	defer in.Close()
	out, err := os.Create(dst)
	if err != nil {
		return
	}
	defer func() {
		cerr := out.Close()
		if err == nil {
			err = cerr
		}
	}()
	if _, err = io.Copy(out, in); err != nil {
		return
	}
	err = out.Sync()
	return
}
