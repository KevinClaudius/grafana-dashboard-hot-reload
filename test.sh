#!/usr/bin/env bash

docker rm grafana
docker rm dashboard-loader
docker-compose up --build
